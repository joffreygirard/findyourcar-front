import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AppComponent } from './app.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'admin',
    loadChildren: () => import('./admin/admin.module')
        .then(module => module.AdminModule)
  },
  {
    path: 'cars',
    loadChildren: () => import('./car/car.module')
        .then(module => module.CarModule)
  },
  {
    path: 'users',
    loadChildren: () => import('./user/user.module')
        .then(module => module.UserModule)
  },
  {
    path: 'garage',
    loadChildren: () => import('./garage/garage.module')
        .then(module => module.GarageModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
