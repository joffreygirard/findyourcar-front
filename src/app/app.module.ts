import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { ResourceModule } from './common/resource/resource.module';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { LoginService } from './login/login.service';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MeService } from './common/me/me.service';
import { AdminModule } from './admin/admin.module';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CarService } from './car/car.service';
import { CarModule } from './car/car.module';
import { UserService } from './user/user.service';
import { UserModule } from './user/user.module';
import { GarageService } from './garage/garage.service';
import { GarageModule } from './garage/garage.module';

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        AdminModule,
        CarModule,
        UserModule,
        GarageModule,
        MatButtonModule,
        MatSidenavModule,
        MatToolbarModule,
        MatIconModule,
        MatListModule,
        ResourceModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatTooltipModule
    ],
    providers: [GarageService, CarService, UserService, LoginService, MeService, MatSnackBar],
    bootstrap: [AppComponent]
})
export class AppModule { }
