import { Component } from '@angular/core';
import { IMeDto } from './common/resource/login/login.dto';
import { MeService } from './common/me/me.service';
import { Observable } from 'rxjs';

interface IMenuItem {
    label: string;
    link: string;
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'FindYourCar';
    me$: Observable<IMeDto | null> = this.meService.getMe();

    menuItems: IMenuItem[] | undefined;

    constructor(private meService: MeService) {

        this.me$.subscribe(
            data => {
                const role: string = String(data?.role);

                if (role === 'ADMIN') {
                        this.menuItems = [
                            {
                                label: '🧔 Mon Compte',
                                link: 'users'
                            },
                            {
                                label: '🏠 Mon Garage',
                                link: 'garage'
                            },
                            {
                                label: '🚗 Voitures',
                                link: 'cars'
                            },
                            {
                                label: '⚙️ Administration',
                                link: 'admin'
                            },
                            {
                                label: '❌ Deconnexion',
                                link: 'logout'
                            }
                        ];
                    }else if (role === 'USER') {
                        this.menuItems = [
                            {
                                label: '🧔 Mon Compte',
                                link: 'users'
                            },
                            {
                                label: '🏠 Mon Garage',
                                link: 'garage'
                            },
                            {
                                label: '🚗 Voitures',
                                link: 'cars'
                            },
                            {
                                label: '❌ Deconnexion',
                                link: 'logout'
                            }
                        ];
                    }else{
                        this.menuItems = [
                            {
                                label: '🔐 Connexion',
                                link: 'login'
                            },
                            {
                                label: '📬 Inscription',
                                link: 'users/edit'
                            },
                            {
                                label: '🚗 Voitures',
                                link: 'cars'
                            }
                        ];
                    }

            }
        );

    }

    logout(): void {
        this.meService.logout();
    }
}
