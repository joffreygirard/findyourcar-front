import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MeGuard } from '../common/me/me.guard';
import { AdminComponent } from './admin.component';
import { CarAdminListComponent } from './car-admin-list/car-admin-list.component';
import { UserAdminListComponent } from './user-admin-list/user-admin-list.component';

const routes: Routes = [
    {
        path: '',
        component: AdminComponent
    },
    {
        // canActivate: [MeGuard],
        path: 'cars',
        component: CarAdminListComponent
    },
    {
        // canActivate: [MeGuard],
        path: 'users',
        component: UserAdminListComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminRoutingModule { }

