import { Component } from '@angular/core';
import { UserService } from '../../user/user.service';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

@Component({
    selector: 'app-user-admin-list',
    templateUrl: './user-admin-list.component.html',
    styleUrls: ['./user-admin-list.component.scss']
})
export class UserAdminListComponent {
    displayedColumns: string[] = ['firstname', 'lastname', 'role', 'login', 'birthdate', 'action'];
    items$ = this.userService.getItems();
    horizontalPosition: MatSnackBarHorizontalPosition = 'center';
    verticalPosition: MatSnackBarVerticalPosition = 'top';

    constructor(private userService: UserService, private snackBar: MatSnackBar) { }

    delete(id: string|null): void {
        if (id) {
            this.userService.deleteItem(id)
                .subscribe(response => {
                    this.items$ = this.userService.getItems();
                    const message = 'Utilisateur supprimée avec succès !';
                    const panelClass = 'success-snack';
                    this.openSnackBar(message, panelClass);
                }, error => {
                    this.items$ = this.userService.getItems();
                });
        }
    }

    openSnackBar(message: string, panelClassName: string): void {
        this.snackBar.open(message, 'Close', {
            duration: 5000,
            panelClass: panelClassName,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
        });
    }

}
