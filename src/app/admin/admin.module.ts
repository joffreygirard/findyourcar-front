import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { AdminRoutingModule } from './admin-routing.module';
import { MatChipsModule } from '@angular/material/chips';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { CarAdminListComponent } from './car-admin-list/car-admin-list.component';
import { MatButtonModule } from '@angular/material/button';
import { UserAdminListComponent } from './user-admin-list/user-admin-list.component';



@NgModule({
    declarations: [
        AdminComponent,
        CarAdminListComponent,
        UserAdminListComponent,
    ],
    imports: [
        CommonModule,
        AdminRoutingModule,
        MatChipsModule,
        MatTableModule,
        MatIconModule,
        MatButtonModule
    ],
    exports: [
        AdminComponent
    ]
})
export class AdminModule { }
