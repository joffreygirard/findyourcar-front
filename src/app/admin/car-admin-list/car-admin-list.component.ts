import { Component } from '@angular/core';
import { CarService } from '../../car/car.service';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

@Component({
    selector: 'app-car-admin-list',
    templateUrl: './car-admin-list.component.html',
    styleUrls: ['./car-admin-list.component.scss']
})
export class CarAdminListComponent {
    displayedColumns: string[] = ['brand', 'model', 'year', 'horsepower', 'price', 'action'];
    items$ = this.carService.getItems();
    horizontalPosition: MatSnackBarHorizontalPosition = 'center';
    verticalPosition: MatSnackBarVerticalPosition = 'top';

    constructor(private carService: CarService, private snackBar: MatSnackBar) { }

    delete(id: string|null): void {
        if (id) {
            this.carService.deleteItem(id)
                .subscribe(response => {
                    this.items$ = this.carService.getItems();
                    const message = 'Voiture supprimée avec succès !';
                    const panelClass = 'success-snack';
                    this.openSnackBar(message, panelClass);
                }, error => {
                    this.items$ = this.carService.getItems();
                });
        }
    }

    openSnackBar(message: string, panelClassName: string): void {
        this.snackBar.open(message, 'Close', {
            duration: 5000,
            panelClass: panelClassName,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
        });
    }

}
