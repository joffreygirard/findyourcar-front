import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarAdminListComponent } from './car-admin-list.component';

describe('CarAdminListComponent', () => {
  let component: CarAdminListComponent;
  let fixture: ComponentFixture<CarAdminListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarAdminListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarAdminListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
