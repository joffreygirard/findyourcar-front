import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { LoginService } from './login.service';
import { AuthTokenService } from '../common/me/auth-token.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent {
    formGroup = new FormGroup({
        login: new FormControl(undefined, Validators.required),
        password: new FormControl(undefined, Validators.required),
    });
    horizontalPosition: MatSnackBarHorizontalPosition = 'center';
    verticalPosition: MatSnackBarVerticalPosition = 'top';

    constructor(private loginService: LoginService, private authService: AuthTokenService, private snackBar: MatSnackBar) {
        this.reset();
    }

    save(): void {
        if (this.formGroup.valid) {
            const credentials = {
                login: this.formGroup.value.login,
                password: this.formGroup.value.password,
            };
            this.loginService.login(credentials)
                .subscribe(token => {
                    this.reset();
                    const message = 'Connecté avec succès !';
                    const panelClass = 'success-snack';
                    this.openSnackBar(message, panelClass);
                }, error => {});
        }
    }

    reset(): void {
        this.formGroup.reset();
    }

    openSnackBar(message: string, panelClassName: string): void {
        this.snackBar.open(message, 'Fermer', {
            duration: 5000,
            panelClass: panelClassName,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
        });
    }

}
