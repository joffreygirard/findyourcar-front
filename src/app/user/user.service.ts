import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserResource } from '../common/resource/user/user.resource';
import { IUser } from './user.model';
import { IUserDto } from '../common/resource/user/user.dto';
import { map } from 'rxjs/operators';

@Injectable()
export class UserService {
    constructor(private userResource: UserResource) {
    }

    getItems(): Observable<IUser[]> {
        return this.userResource.findAll()
            .pipe(
                map((dtos: IUserDto[]) => {
                    return dtos.map(this.dtoToModel);
                })
            );
    }

    getItem(id: string): Observable<IUser> {
        return this.userResource.get(id)
            .pipe(
                map(dto => this.dtoToModel(dto))
            );
    }

    saveItem(user: IUser): Observable<IUser> {
        const userDto = this.modelToDto(user);
        const save$ = userDto.id
            ? this.userResource.update(userDto)
            : this.userResource.create(userDto);
        return save$
            .pipe(
                map(dto => this.dtoToModel(dto))
            );
    }

    deleteItem(id: string): Observable<number> {
        return this.userResource.delete(id);
    }

    private dtoToModel(dto: IUserDto): IUser {
        return {
            id: dto.id,
            login: dto.login,
            password: dto.password,
            firstname: dto.firstname,
            lastname: dto.lastname,
            role: dto.role,
            birthdate: dto.birthdate,
        };
    }

    private modelToDto(model: IUser): IUserDto {
        return {
            id: model.id,
            login: model.login,
            password: model.password,
            firstname: model.firstname,
            lastname: model.lastname,
            role: model.role,
            birthdate: model.birthdate,
        };
    }

}
