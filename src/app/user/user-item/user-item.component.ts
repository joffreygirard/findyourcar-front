/* tslint:disable:no-trailing-whitespace */
import { Component, Input } from '@angular/core';
import { IUser } from '../user.model';
import { UserService} from '../user.service';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import {MeService} from '../../common/me/me.service';

@Component({
    selector: 'app-user-item',
    templateUrl: './user-item.component.html',
    styleUrls: ['./user-item.component.scss']
})
export class UserItemComponent {
    horizontalPosition: MatSnackBarHorizontalPosition = 'center';
    verticalPosition: MatSnackBarVerticalPosition = 'top';

    @Input() item: IUser | undefined | null;

    constructor(private userService: UserService, private snackBar: MatSnackBar, private meService: MeService) {
    }

    delete(id: string|null): void {
        if (id) {
            this.userService.deleteItem(id)
                .subscribe(response => {
                    const message = 'Utilisateur supprimé avec succès !';
                    const panelClass = 'success-snack';
                    this.openSnackBar(message, panelClass);
                    this.meService.logout();
                }, error => {
                });
        }
    }

    openSnackBar(message: string, panelClassName: string): void {
        this.snackBar.open(message, 'Close', {
            duration: 5000,
            panelClass: panelClassName,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
        });
    }
}
