/* tslint:disable:no-trailing-whitespace */
import { Component } from '@angular/core';
import { UserService } from './user.service';
import { MeService } from '../common/me/me.service';
import { IMeDto } from '../common/resource/login/login.dto';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss'],
    providers: [
        UserService
    ]
})
export class UserComponent {
    me$: Observable<IMeDto | null> = this.meService.getMe();
    
    item$ = this.service.getItem('607a0bf3ce5db61a142a52f9');

    constructor(private service: UserService, private meService: MeService) {
        this.me$.subscribe(
            data => {
                const id: string = String(data?.id);
                this.item$ = this.service.getItem(id);
            }
        );
        
    }
}

