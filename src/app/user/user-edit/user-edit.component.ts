import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map, switchMap, tap } from 'rxjs/operators';
import { UserService } from '../user.service';
import { IUser } from '../user.model';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { DatePipe } from '@angular/common';
import { GarageService } from '../../garage/garage.service';
import { IGarage } from '../../garage/garage.model';

@Component({
    selector: 'app-user-edit',
    templateUrl: './user-edit.component.html',
    styleUrls: ['./user-edit.component.scss'],
    providers: [
        UserService,
        DatePipe
    ]
})
export class UserEditComponent {
    selected = 'USER';
    formGroup: FormGroup;
    user$: Observable<IUser>;
    horizontalPosition: MatSnackBarHorizontalPosition = 'center';
    verticalPosition: MatSnackBarVerticalPosition = 'top';

    constructor(
        private userService: UserService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private snackBar: MatSnackBar,
        private router: Router,
        private datepipe: DatePipe,
        private garageService: GarageService,
    ) {
        this.formGroup = this.formBuilder.group({
            id: [],
            login: [undefined, Validators.required],
            password: [undefined, Validators.required],
            firstname: [undefined, Validators.required],
            lastname: [undefined, Validators.required],
            role: [undefined, Validators.required],
            birthdate: [undefined, Validators.required],
        });
        this.reset();
        this.user$ = this.route.params
            .pipe(
                map(params => params.id),
                filter(id => !!id),
                switchMap(id => this.userService.getItem(id)),
                tap(user => {
                    this.formGroup.patchValue({
                        id: user.id,
                        login: user.login,
                        password: user.password,
                        firstname: user.firstname,
                        lastname: user.lastname,
                        role: user.role,
                        birthdate: this.datepipe.transform(user.birthdate, 'yyyy-MM-dd')
                    });
                }));
    }

    save(): void {
        if (this.formGroup.valid) {
            const user: IUser = {
                id: this.formGroup.value.id,
                login: this.formGroup.value.login,
                password: this.formGroup.value.password,
                firstname: this.formGroup.value.firstname,
                lastname: this.formGroup.value.lastname,
                role: this.formGroup.value.role,
                birthdate: this.formGroup.value.birthdate,
            };
            this.userService.saveItem(user)
                .subscribe(createdUser => {
                    const message = 'Modifications effectuées avec succès !';
                    const panelClass = 'success-snack';
                    this.openSnackBar(message, panelClass);

                    if (user.id) {
                        this.router.navigate(['/users']);
                    } else {
                        const userId: string = String(createdUser?.id);
                        const garage: IGarage = {
                            user_id: userId,
                            cars: []
                        };
                        this.garageService.saveItem(garage)
                            .subscribe(createdGarage => {
                                this.router.navigate(['/login']);
                            });
                    }
                }, error => {});
        }
    }

    reset(): void {
        this.formGroup.reset({
            birthdate: '1970-01-01'
        });
    }

    openSnackBar(message: string, panelClassName: string): void {
        this.snackBar.open(message, 'Close', {
            duration: 5000,
            panelClass: panelClassName,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
        });
    }

}
