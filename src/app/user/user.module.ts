import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatChipsModule } from '@angular/material/chips';
import { MatListModule } from '@angular/material/list';
import { UserItemComponent } from './user-item/user-item.component';
import { UserComponent } from './user.component';
import { MatCardModule } from '@angular/material/card';
import { UserEditComponent } from './user-edit/user-edit.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { UserRoutingModule } from './user-routing.module';
import {FlexModule} from '@angular/flex-layout';
import {MatSelectModule} from '@angular/material/select';

@NgModule({
  declarations: [
      UserComponent,
      UserItemComponent,
      UserEditComponent
  ],
    imports: [
        CommonModule,
        UserRoutingModule,
        MatListModule,
        MatChipsModule,
        MatCardModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatButtonModule,
        RouterModule,
        MatGridListModule,
        MatSnackBarModule,
        FlexModule,
        MatSelectModule

    ],
  exports: [
      UserComponent
  ]
})
export class UserModule { }
