export interface IGarage {
    id?: string;
    user_id: string;
    cars: ICarGarage[];
}

export interface ICarGarage {
    _id?: string;
    brand: string;
    model: string;
    year: number;
    image: string;
    price: number;
    horsepower: string;
}
