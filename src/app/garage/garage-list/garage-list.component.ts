import {Component, EventEmitter, Input, Output} from '@angular/core';
import { ICarGarage } from '../garage.model';

@Component({
    selector: 'app-garage-list',
    templateUrl: './garage-list.component.html',
    styleUrls: ['./garage-list.component.scss']
})
export class GarageListComponent {

    @Input() items: ICarGarage[] | undefined | null;
    @Output() deleteCar = new EventEmitter();

    constructor() {
    }

    deleteItem($event: any): void {
        this.deleteCar.emit({ carId: $event.carId});
    }
}
