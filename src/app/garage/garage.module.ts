import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatChipsModule } from '@angular/material/chips';
import { MatListModule } from '@angular/material/list';
import { GarageItemComponent } from './garage-item/garage-item.component';
import { GarageListComponent } from './garage-list/garage-list.component';
import { GarageComponent } from './garage.component';
import { MatCardModule } from '@angular/material/card';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { GarageRoutingModule } from './garage-routing.module';
import {FlexModule} from '@angular/flex-layout';

@NgModule({
  declarations: [
      GarageComponent,
      GarageItemComponent,
      GarageListComponent
  ],
    imports: [
        CommonModule,
        GarageRoutingModule,
        MatListModule,
        MatChipsModule,
        MatCardModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatButtonModule,
        RouterModule,
        MatGridListModule,
        MatSnackBarModule,
        FlexModule
    ],
  exports: [
      GarageComponent
  ]
})
export class GarageModule { }

