import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GarageResource } from '../common/resource/garage/garage.resource';
import { IGarage, ICarGarage } from './garage.model';
import { IGarageDto } from '../common/resource/garage/garage.dto';
import { map } from 'rxjs/operators';

@Injectable()
export class GarageService {
    constructor(private garageResource: GarageResource) {
    }

    getCars(userId: string): Observable<ICarGarage[]> {
        return this.getItem(userId)
            .pipe(
                map(garage => {
                    return garage.cars;
                })
            );
    }

    getItem(id: string): Observable<IGarage> {
        return this.garageResource.get(id)
            .pipe(
                map(dto => this.dtoToModel(dto))
            );
    }

    saveItem(garage: IGarage): Observable<IGarage> {
        const garageDto = this.modelToDto(garage);
        const save$ = garageDto.id
            ? this.garageResource.update(garageDto)
            : this.garageResource.create(garageDto);
        return save$
            .pipe(
                map(dto => this.dtoToModel(dto))
            );
    }

    deleteItem(id: string): Observable<number> {
        return this.garageResource.delete(id);
    }

    private dtoToModel(dto: IGarageDto): IGarage {
        return {
            id: dto.id,
            user_id: dto.user_id,
            cars: dto.cars,
        };
    }

    private modelToDto(model: IGarage): IGarageDto {
        return {
            id: model.id,
            user_id: model.user_id,
            cars: model.cars,
        };
    }

}
