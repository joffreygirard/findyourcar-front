/* tslint:disable:no-trailing-whitespace */
import { Component } from '@angular/core';
import { GarageService } from './garage.service';
import { Observable } from 'rxjs';
import { IMeDto } from '../common/resource/login/login.dto';
import { ICarGarage, IGarage } from './garage.model';
import { MeService } from '../common/me/me.service';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

@Component({
    selector: 'app-garage',
    templateUrl: './garage.component.html',
    styleUrls: ['./garage.component.scss'],
    providers: [
        GarageService
    ]
})
export class GarageComponent {
    horizontalPosition: MatSnackBarHorizontalPosition = 'center';
    verticalPosition: MatSnackBarVerticalPosition = 'top';
    me$: Observable<IMeDto | null> = this.meService.getMe();
    items$: Observable<ICarGarage[] | null> = this.service.getCars('');

    constructor(private service: GarageService, private meService: MeService, private snackBar: MatSnackBar) {
        this.refreshCarsList();
    }

    deleteCar($event: any): void {
        const carId = $event.carId;
        this.me$.subscribe(
            data => {
                const userId: string = String(data?.id);
                this.service.getItem(userId)
                    .subscribe(garage => {
                        // tslint:disable-next-line:only-arrow-functions typedef
                        const newCars = garage.cars.filter(function(
                            car: ICarGarage) {
                            return car._id !== carId;
                        });

                        const myGarage: IGarage = {
                            id: garage.id,
                            user_id: garage.user_id,
                            cars: newCars
                        };
                        this.service.saveItem(myGarage)
                            .subscribe(response => {
                                this.refreshCarsList();
                                const message = 'Voiture supprimée avec succès !';
                                const panelClass = 'success-snack';
                                this.openSnackBar(message, panelClass);
                            });

                    });
            }
        );
    }

    refreshCarsList(): void {
        this.me$.subscribe(
            data => {
                const userId: string = String(data?.id);
                this.items$ = this.service.getCars(userId);
            }
        );
    }

    openSnackBar(message: string, panelClassName: string): void {
        this.snackBar.open(message, 'Close', {
            duration: 5000,
            panelClass: panelClassName,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
        });
    }

}

