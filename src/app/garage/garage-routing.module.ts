import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GarageComponent } from './garage.component';
import { MeGuard } from '../common/me/me.guard';

const routes: Routes = [
    {
        canActivate: [MeGuard],
        path: '',
        component: GarageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GarageRoutingModule { }

