/* tslint:disable:no-trailing-whitespace */
import { Component, EventEmitter, Input, Output, } from '@angular/core';
import { ICarGarage } from '../garage.model';

@Component({
    selector: 'app-garage-item',
    templateUrl: './garage-item.component.html',
    styleUrls: ['./garage-item.component.scss']
})
export class GarageItemComponent {

    @Input() item: ICarGarage | undefined;
    @Output() deleteItem = new EventEmitter();

    constructor() {
    }

    delete(id: string|null): void {
        this.deleteItem.emit({ carId: id });
    }
}
