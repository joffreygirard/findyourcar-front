import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import {MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition} from '@angular/material/snack-bar';

enum  ErrorType {
    resourceIdFormat = 'RESOURCE_ID_FORMAT',
    resourceIdNotFound = 'RESOURCE_ID_NOT_FOUND',
    resourceTypeNotFound = 'RESOURCE_TYPE_NOT_FOUND',
    unhandledError = 'UNHANDLED_ERROR',
    invalidCredentials = 'INVALID_CREDENTIALS',
    missingToken = 'MISSING_TOKEN',
    invalidToken = 'INVALID_TOKEN',
    missingRole = 'MISSING_ROLE'
}

@Injectable()
export class ErrorHandlerInterceptor implements HttpInterceptor {
    horizontalPosition: MatSnackBarHorizontalPosition = 'center';
    verticalPosition: MatSnackBarVerticalPosition = 'top';

    constructor(private snackbarService: MatSnackBar) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req)
            .pipe(
                catchError(error => {
                    const message = this.getMessage(error.error?.type);
                    const panelClass = 'error-snack';
                    this.openSnackBar(message, panelClass);
                    return throwError(error);
                })
            );
    }

    private getMessage(errorType: ErrorType): string {
        switch (errorType) {
            case ErrorType.missingRole:
                return 'Vous n\'avez pas les droits !';
            case ErrorType.invalidCredentials:
                return 'Login ou mot de passe incorrect';
            case ErrorType.invalidToken:
                return 'Le token est invalide ! Veuillez vous reconnecter';
            case ErrorType.missingToken:
                return 'Vous devez être connecté !';
            case ErrorType.resourceIdFormat:
                return 'Veuillez respecter le format d\'id !';
            case ErrorType.resourceIdNotFound:
                return 'Aucun objet trouvé avec cet id';
            case ErrorType.resourceTypeNotFound:
                return 'La ressource que vous avez demandé n\'existe pas !';
            case ErrorType.unhandledError:
                return 'Une erreur est survenue';
            default:
                return 'Une erreur est survenue';
        }
    }

    openSnackBar(message: string, panelClassName: string): void {
        this.snackbarService.open(message, 'Close', {
            duration: 5000,
            panelClass: panelClassName,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
        });
    }
}
