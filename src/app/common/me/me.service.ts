import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { IMeDto, ITokenDto } from '../resource/login/login.dto';
import { LoginResource } from '../resource/login/login.resource';
import { AuthTokenService } from './auth-token.service';
import { Router } from '@angular/router';

@Injectable()
export class MeService {

    private me$ = new BehaviorSubject<IMeDto | null>(null);

    constructor(
        private authTokenService: AuthTokenService,
        private loginResource: LoginResource,
        private router: Router
    ) {
        if (this.authTokenService.getToken()) {
            this.setMe();
        }
    }

    login(tokenDto: ITokenDto): void {
        this.authTokenService.setToken(tokenDto.token);
        this.setMe();
    }

    private setMe(): void {
        this.loginResource.me()
            .subscribe(meDto => {
                this.me$.next(meDto);
            });
    }

    getMe(): Observable<IMeDto | null> {
        return this.me$.asObservable();
    }

    logout(): void {
        this.authTokenService.clearToken();
        this.me$.next(null);
        this.router.navigate(['/login']);
    }
}
