import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { MeService } from './me.service';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class MeGuard implements CanActivate {
    constructor(
        private meService: MeService
    ) {
    }
    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> {
        return this.meService.getMe()
            .pipe(
                map(meDto => !!meDto)
            );
    }
}
