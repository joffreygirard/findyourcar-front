import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ICarDto } from './car.dto';

@Injectable()
export class CarResource {

    private url = 'http://localhost:3000/api/cars';

    constructor(
        private http: HttpClient
    ) {
    }

    findAll(): Observable<ICarDto[]> {
        return this.http.get<ICarDto[]>(this.url);
    }

    get(id: string): Observable<ICarDto> {
        return this.http.get<ICarDto>(`${this.url}/${id}`);
    }

    create(car: ICarDto): Observable<ICarDto> {
        return this.http.post<ICarDto>(this.url, car);
    }

    update(dto: ICarDto): Observable<ICarDto> {
        return this.http.put<ICarDto>(`${this.url}/${dto.id}`, dto);
    }

    delete(id: string): Observable<number> {
        const deleteUrl = this.url + '/' + id;
        return this.http.delete<number>(deleteUrl);
    }
}
