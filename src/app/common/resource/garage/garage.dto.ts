export interface IGarageDto {
    id?: string;
    user_id: string;
    cars: ICarGarageDto[];
}

export interface ICarGarageDto {
    _id?: string;
    brand: string;
    model: string;
    year: number;
    image: string;
    price: number;
    horsepower: string;
}
