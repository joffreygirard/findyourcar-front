import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IGarageDto } from './garage.dto';

@Injectable()
export class GarageResource {

    private url = 'http://localhost:3000/api/garages';

    constructor(
        private http: HttpClient
    ) {
    }

    get(id: string): Observable<IGarageDto> {
        return this.http.get<IGarageDto>(`${this.url}/findByUser/${id}`);
    }

    create(garage: IGarageDto): Observable<IGarageDto> {
        return this.http.post<IGarageDto>(this.url, garage);
    }

    update(dto: IGarageDto): Observable<IGarageDto> {
        return this.http.put<IGarageDto>(`${this.url}/${dto.id}`, dto);
    }

    delete(id: string): Observable<number> {
        const deleteUrl = this.url + '/' + id;
        return this.http.delete<number>(deleteUrl);
    }
}
