export interface ICredentialsDto {
    login: string;
    password: string;
}

export interface ITokenDto {
    token: string;
}

export enum UserRole {
    user = 'USER',
    admin = 'ADMIN'
}

export interface IMeDto {
    id: string;
    login: string;
    firstname: string;
    lastname: string;
    birthdate: string;
    role: string;
}
