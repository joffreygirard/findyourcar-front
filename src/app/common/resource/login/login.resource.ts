import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ICredentialsDto, IMeDto, ITokenDto } from './login.dto';

@Injectable()
export class LoginResource {

    private url = 'http://localhost:3000/api/auth';

    constructor(
        private http: HttpClient
    ) {
    }

    login(credentials: ICredentialsDto): Observable<ITokenDto> {
        return this.http.post<ITokenDto>(this.url + '/login', credentials);
    }

    me(): Observable<IMeDto> {
        return this.http.get<IMeDto>(this.url + '/me');
    }
}
