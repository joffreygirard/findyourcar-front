export interface IUserDto {
    id?: string;
    login: string;
    password: string;
    firstname: string;
    lastname: string;
    role: string;
    birthdate: string;
}
