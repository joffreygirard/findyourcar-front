import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IUserDto } from './user.dto';

@Injectable()
export class UserResource {

    private url = 'http://localhost:3000/api/users';

    constructor(
        private http: HttpClient
    ) {
    }

    findAll(): Observable<IUserDto[]> {
        return this.http.get<IUserDto[]>(this.url);
    }

    get(id: string): Observable<IUserDto> {
        return this.http.get<IUserDto>(`${this.url}/${id}`);
    }

    create(user: IUserDto): Observable<IUserDto> {
        return this.http.post<IUserDto>(this.url, user);
    }

    update(dto: IUserDto): Observable<IUserDto> {
        return this.http.put<IUserDto>(`${this.url}/${dto.id}`, dto);
    }

    delete(id: string): Observable<number> {
        const deleteUrl = this.url + '/' + id;
        return this.http.delete<number>(deleteUrl);
    }
}
