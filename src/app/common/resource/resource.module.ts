import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AuthTokenService } from '../me/auth-token.service';
import { AuthenticationInterceptor } from '../interceptor/authentication.interceptor';
import { ErrorHandlerInterceptor } from '../interceptor/error-handler.interceptor';
import { LoginResource } from './login/login.resource';
import { CarResource } from './car/car.resource';
import { UserResource } from './user/user.resource';
import { GarageResource } from './garage/garage.resource';

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule
    ],
    providers: [
        LoginResource,
        AuthTokenService,
        CarResource,
        UserResource,
        GarageResource,
        { provide: HTTP_INTERCEPTORS, useClass: AuthenticationInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorHandlerInterceptor, multi: true }
    ]
})
export class ResourceModule { }
