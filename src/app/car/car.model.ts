export interface ICar {
    id?: string;
    brand: string;
    model: string;
    year: number;
    image: string;
    price: number;
    horsepower: string;
}
