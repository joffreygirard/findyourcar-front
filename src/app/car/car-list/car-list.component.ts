import { Component, Input } from '@angular/core';
import { ICar } from '../car.model';
import { CarService } from '../car.service';
import { MeService } from '../../common/me/me.service';
import { IMeDto } from '../../common/resource/login/login.dto';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-car-list',
    templateUrl: './car-list.component.html',
    styleUrls: ['./car-list.component.scss']
})
export class CarListComponent {

    @Input() items: ICar[] | undefined | null;

    me$: Observable<IMeDto | null> = this.meService.getMe();
    role: string | undefined

    constructor(private carService: CarService, private meService: MeService) {

        this.me$.subscribe(
            data => {
                this.role = String(data?.role);
            }
        );

    }

    refreshItemsList(): void {
        this.carService.getItems()
            .subscribe(cars => {
                this.items = cars;
            });
    }
}
