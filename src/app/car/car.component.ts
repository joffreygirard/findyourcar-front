/* tslint:disable:no-trailing-whitespace */
import { Component } from '@angular/core';
import { CarService } from './car.service';

@Component({
    selector: 'app-car',
    templateUrl: './car.component.html',
    styleUrls: ['./car.component.scss'],
    providers: [
        CarService
    ]
})
export class CarComponent {
    items$ = this.service.getItems();

    constructor(private service: CarService) {

    }
}

