import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CarComponent } from './car.component';
import { CarEditComponent } from './car-edit/car-edit.component';
import { MeGuard } from '../common/me/me.guard';

const routes: Routes = [
    {
        path: '',
        component: CarComponent
    },
    {
        canActivate: [MeGuard],
        path: 'edit',
        component: CarEditComponent
    },
    {
        canActivate: [MeGuard],
        path: 'edit/:id',
        component: CarEditComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CarRoutingModule { }

