import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map, switchMap, tap } from 'rxjs/operators';
import { CarService } from '../car.service';
import { ICar } from '../car.model';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

@Component({
    selector: 'app-car-edit',
    templateUrl: './car-edit.component.html',
    styleUrls: ['./car-edit.component.scss'],
    providers: [
        CarService
    ]
})
export class CarEditComponent {

    formGroup: FormGroup;
    car$: Observable<ICar>;
    horizontalPosition: MatSnackBarHorizontalPosition = 'center';
    verticalPosition: MatSnackBarVerticalPosition = 'top';

    constructor(
        private carService: CarService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private snackBar: MatSnackBar,
        private router: Router
    ) {
        this.formGroup = this.formBuilder.group({
            id: [],
            brand: [undefined, Validators.required],
            model: [undefined, Validators.required],
            year: [undefined, Validators.required],
            image: [undefined, Validators.required],
            price: [undefined, Validators.required],
            horsepower: [undefined, Validators.required],
        });
        this.reset();
        this.car$ = this.route.params
            .pipe(
                map(params => params.id),
                filter(id => !!id),
                switchMap(id => this.carService.getItem(id)),
                tap(car => {
                    this.formGroup.patchValue({
                        id: car.id,
                        brand: car.brand,
                        model: car.model,
                        year: car.year,
                        image: car.image,
                        price: car.price,
                        horsepower: car.horsepower
                    });
                }));
    }

    save(): void {
        if (this.formGroup.valid) {
            const car: ICar = {
                id: this.formGroup.value.id,
                brand: this.formGroup.value.brand,
                model: this.formGroup.value.model,
                year: this.formGroup.value.year,
                image: this.formGroup.value.image,
                price: this.formGroup.value.price,
                horsepower: this.formGroup.value.horsepower,
            };
            this.carService.saveItem(car)
                .subscribe(createdCar => {
                    const message = 'Modifications effectuées avec succès !';
                    const panelClass = 'success-snack';
                    this.openSnackBar(message, panelClass);
                    this.router.navigate(['/cars']);
                }, error => {});
        }
    }

    reset(): void {
        this.formGroup.reset({
            year: 2000
        });
    }

    openSnackBar(message: string, panelClassName: string): void {
        this.snackBar.open(message, 'Close', {
            duration: 5000,
            panelClass: panelClassName,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
        });
    }

}
