import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatChipsModule } from '@angular/material/chips';
import { MatListModule } from '@angular/material/list';
import { CarItemComponent } from './car-item/car-item.component';
import { CarListComponent } from './car-list/car-list.component';
import { CarComponent } from './car.component';
import { MatCardModule } from '@angular/material/card';
import { CarEditComponent } from './car-edit/car-edit.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CarRoutingModule } from './car-routing.module';
import {FlexModule} from '@angular/flex-layout';

@NgModule({
  declarations: [
      CarComponent,
      CarItemComponent,
      CarListComponent,
      CarEditComponent
  ],
    imports: [
        CommonModule,
        CarRoutingModule,
        MatListModule,
        MatChipsModule,
        MatCardModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatButtonModule,
        RouterModule,
        MatGridListModule,
        MatSnackBarModule,
        FlexModule
    ],
  exports: [
      CarComponent
  ]
})
export class CarModule { }

