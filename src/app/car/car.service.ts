import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CarResource } from '../common/resource/car/car.resource';
import { ICar } from './car.model';
import { ICarDto } from '../common/resource/car/car.dto';
import { map } from 'rxjs/operators';

@Injectable()
export class CarService {
    constructor(private carResource: CarResource) {
    }

    getItems(): Observable<ICar[]> {
        return this.carResource.findAll()
            .pipe(
                map((dtos: ICarDto[]) => {
                    return dtos.map(this.dtoToModel);
                })
            );
    }

    getItem(id: string): Observable<ICar> {
        return this.carResource.get(id)
            .pipe(
                map(dto => this.dtoToModel(dto))
            );
    }

    saveItem(car: ICar): Observable<ICar> {
        const carDto = this.modelToDto(car);
        const save$ = carDto.id
            ? this.carResource.update(carDto)
            : this.carResource.create(carDto);
        return save$
            .pipe(
                map(dto => this.dtoToModel(dto))
            );
    }

    deleteItem(id: string): Observable<number> {
        return this.carResource.delete(id);
    }

    private dtoToModel(dto: ICarDto): ICar {
        return {
            id: dto.id,
            brand: dto.brand,
            model: dto.model,
            year: dto.year,
            image: dto.image,
            price: dto.price,
            horsepower: dto.horsepower,
        };
    }

    private modelToDto(model: ICar): ICarDto {
        return {
            id: model.id,
            brand: model.brand,
            model: model.model,
            year: model.year,
            image: model.image,
            price: model.price,
            horsepower: model.horsepower,
        };
    }

}
