/* tslint:disable:no-trailing-whitespace */
import { Component, EventEmitter, Input, Output, } from '@angular/core';
import { ICar } from '../car.model';
import { CarService} from '../car.service';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MeService } from '../../common/me/me.service';
import { IMeDto } from '../../common/resource/login/login.dto';
import { Observable } from 'rxjs';
import { ICarGarage, IGarage } from '../../garage/garage.model';
import { GarageService } from '../../garage/garage.service';

@Component({
    selector: 'app-car-item',
    templateUrl: './car-item.component.html',
    styleUrls: ['./car-item.component.scss']
})
export class CarItemComponent {
    horizontalPosition: MatSnackBarHorizontalPosition = 'center';
    verticalPosition: MatSnackBarVerticalPosition = 'top';

    @Input() item: ICar | undefined;
    @Output() refreshItemsList = new EventEmitter();

    me$: Observable<IMeDto | null> = this.meService.getMe();
    role: string | undefined;

    constructor(private carService: CarService,
                private snackBar: MatSnackBar,
                private meService: MeService,
                private garageService: GarageService
    ) {
        this.me$.subscribe(
            data => {
                this.role = String(data?.role);
            }
        );
    }

    add(item: ICar|null): void {
        if (item) {
            this.me$.subscribe(
                data => {
                    const userId: string = String(data?.id);
                    this.garageService.getItem(userId)
                        .subscribe(garage => {
                            // tslint:disable-next-line:only-arrow-functions typedef
                            const previousCars = garage.cars.filter(function(
                                car: ICarGarage) {
                                return car._id === item.id;
                            });

                            if (previousCars.length) {
                                const message = 'Vous avez déjà ajouté cette voiture dans votre garage !';
                                const panelClass = 'success-snack';
                                this.openSnackBar(message, panelClass);
                            } else {

                                const newCar = {
                                    _id: item.id,
                                    brand: item.brand,
                                    model: item.model,
                                    year: item.year,
                                    image: item.image,
                                    price: item.price,
                                    horsepower: item.horsepower
                                };

                                const newCars = garage.cars;
                                newCars.push(newCar);

                                const myGarage: IGarage = {
                                    id: garage.id,
                                    user_id: garage.user_id,
                                    cars: newCars
                                };
                                this.garageService.saveItem(myGarage)
                                    .subscribe(response => {
                                        const message = 'Voiture ajoutée avec succès !';
                                        const panelClass = 'success-snack';
                                        this.openSnackBar(message, panelClass);
                                    });
                            }
                        });
                });
        }
    }

    delete(id: string|null): void {
        if (id) {
            this.carService.deleteItem(id)
                .subscribe(response => {
                    this.refreshItemsList.emit();
                    const message = 'Voiture supprimée avec succès !';
                    const panelClass = 'success-snack';
                    this.openSnackBar(message, panelClass);
                }, error => {
                    this.refreshItemsList.emit();
                });
        }
    }

    openSnackBar(message: string, panelClassName: string): void {
        this.snackBar.open(message, 'Close', {
            duration: 5000,
            panelClass: panelClassName,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
        });
    }
}
