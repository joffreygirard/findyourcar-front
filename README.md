# FindYourCarFront

Projet réalisé par Joffrey Girard, Delabarre Aymeric et Parlier Valentin.

## Sujet

Notre avons décidé de réaliser un garage connecté avec la possibilité d'y ajouter les dernières voitures à la mode dès lors que l'utilisateur est connecté à notre application. 

## Couches techniques

Nous sommes partis sur la même architecture que nous avons réalisé lors du cours de WebFullStack avec le appComposent en tant que composant principal de l'application. Puis, différents composants que nous avons rajoutés comme le système de "car" ou encore le "login". Nous avons par la suite relier tout ses composants avec les routes adéquates.

S'il était nécessaire, pour le composant en question, nous avons réalisé des sous-composants qui permettent nottament l'affichage des pages de liste et d'edition. Par exemple, le composant "car" possède 3 sous-somposants, le composant "CarItemComponent", le composant "CarEditComponent" et le "CarListComponent".

## Intercepteurs

Nous avons utilisé 2 intercepteurs différents dans ce projet. Le premier est un intercepteur d'authentification qui catch les requêtes HTTP et vérifie si l'utilisateur est connecté et si c'est le cas, rajoute dans le header un "Bearer" token spécifique à l'utilisateur connecté.

Puis un second intercepteur d'erreur celui-ci, qui recupère le retour des requêtes HTTP et vérifie si la requête contient des erreurs et si c'est le cas, affiche un snackbar avec le nom de l'erreur correspondant.




## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
